import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.IntBinaryOperator;

public class AtomicTest {


    public void AtomicIncrement() {
        ExecutorService executorService = null;
        Counter counter = new Counter();
        try {

            executorService = Executors.newFixedThreadPool(2);

            Runnable task1 = () -> {
                for (int i = 1; i <= 20000; i++) {
                    counter.incrementCounter();
                }
            };
            Runnable task2 = () -> {
                for (int i = 1; i <= 20000; i++) {
                    counter.incrementCounter();
                }
            };

            executorService.submit(task1);
            executorService.submit(task2);
            executorService.awaitTermination(1, TimeUnit.SECONDS);
            System.out.println("Increment Counter value = " + counter.getCounter());
        } catch (Exception e) {

        } finally {
            if (executorService != null) {
                executorService.shutdown();
            }
        }

    }
    public void AtomicDecrement() {
        ExecutorService executorService = null;
        Counter counter = new Counter();
        try {
            executorService = Executors.newFixedThreadPool(2);

            Runnable task1 = () -> {
                for (int i = 1; i <= 20000; i++) {
                    counter.decrementCounter();
                }
            };
            Runnable task2 = () -> {
                for (int i = 1; i <= 20000; i++) {
                    counter.decrementCounter();
                }
            };

            executorService.submit(task1);
            executorService.submit(task2);
            executorService.awaitTermination(1, TimeUnit.SECONDS);
            System.out.println("Decrement Counter value = " + counter.getCounter());
        } catch (Exception e) {

        } finally {
            if (executorService != null) {
                executorService.shutdown();
            }
        }

    }

    public void AtomicGetAndSet(){
        Counter counter = new Counter();
        counter.setCounter(100);
        System.out.println("GetAndSet Counter first value = " + counter.getCounter());
        int newCounter = 555;
        counter.getAndSetCounter(newCounter);
        System.out.println("GetAndSet Counter second value = " + counter.getCounter());
    }

    public void AtomicAddAndGet(){
        Counter counter = new Counter();
        counter.setCounter(50);
        int delta = 8;
        System.out.println("AddAndGet Counter first value = " + counter.getCounter());
        counter.getAndAddCounter(delta);
        System.out.println("AddAndGet Counter second value = " + counter.getCounter());
    }

    public void AtomicCompareAndSet() {
        Counter counter = new Counter();
        int counterInitValue = counter.getCounter();
        int expected = 0;
        int update = 100;
        boolean result = counter.compareAndSetCounter(expected,update);
        System.out.println("Counter expected value = " + counterInitValue);
        System.out.println("Counter new value = " + counter.getCounter());
        System.out.println("Compare and set value = " + result);
    }

    public void AtomicAccAndGet() {
        Counter counter = new Counter();
        counter.setCounter(5);
        IntBinaryOperator function = (a,b)->(a+b);
        int value = 2;
        counter.z(value, function);
        System.out.println("Accumulate and get Counter value = " + counter.getCounter());
    }
    public static void main(String[] args) {
        AtomicTest atomicTest = new AtomicTest();
        atomicTest.AtomicIncrement();
        atomicTest.AtomicDecrement();
        atomicTest.AtomicGetAndSet();
        atomicTest.AtomicAddAndGet();
        atomicTest.AtomicCompareAndSet();
        atomicTest.AtomicAccAndGet();
    }
}
